# mlops-101

The repo for the "MLOps from the ground up" series.

Stream: https://www.twitch.tv/adhitads.

## What is the stream about?

We introduce standardized tools for MLOps - accelerating ML model development and deployment, walk through them as we set it up for a real-world project. 
